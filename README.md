# [Survey Web](https://survey.nexti.dev)

This is the react version of the Survey App.

This application consumes the [Survey API](https://gitlab.com/nexti-survey/survey-api) Project.

## Execution

To execute this project into your machine, you need to download the project, and follow the next steps:

* Download all dependencies:
  ```
  ~$ yarn 
  ```

* Edit the .env and replace all values to valid ones. Ex:
    ```
    REACT_APP_ENV=beta
    ```

    > *Note: The **beta** env will try to consume the API on localhost:3100 and the **production** will try to consume https://api.survey.nexti.dev*

* Now you can execute it:
  ```
  ~$ yarn start
  ```

