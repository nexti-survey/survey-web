import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import Answers from "./Components/Answers";
import Header from "./Components/Header";
import ProtectedRoute from "./Components/Helper/ProtectedRoute";
import Home from "./Components/Home";
import Login from "./Components/Login";
import { UserStorage } from "./UserContext";

function App() {
  return (
    <div>
      <BrowserRouter>
        <UserStorage>
          <Header />
          <Switch>
            <Route path="/login" component={Login} />
            <ProtectedRoute path="/answers/:questionId" component={Answers} />
            <ProtectedRoute path="/" component={Home} />
          </Switch>
        </UserStorage>
      </BrowserRouter>
    </div>
  );
}

export default App;
