import React from "react";
import { useParams } from "react-router";
import { ANSWERS_GET, QUESTION_GET } from "../api";
import useFetch from "../Hooks/useFetch";
import Error from "./Helper/Error";

import styles from "./Answers.module.css";

const Answers = () => {
  const { questionId } = useParams();

  const [question, setQuestion] = React.useState(null);
  const [answers, setAnswers] = React.useState(null);

  const { error, request } = useFetch();

  React.useEffect(() => {
    async function getQuestionDetails() {
      const { url, options } = QUESTION_GET(questionId);

      const { json } = await request(url, options);

      setQuestion(json);
    }

    async function getAnswers() {
      const { url, options } = ANSWERS_GET(questionId);

      const { json } = await request(url, options);

      setAnswers(json);
    }

    let interval = setInterval(() => {
      getAnswers();
    }, 5000);

    getQuestionDetails();
    getAnswers();

    return () => {
      clearInterval(interval);
    };
  }, []);

  if (error) return <Error error={error} />;
  if (question && answers)
    return (
      <section className="container mainContainer">
        <h1 className="title">{question.question}</h1>
        <h3 className="subtitle">Answers:</h3>
        {answers.map((answer) => (
          <div className={styles.answer} key={answer._id}>
            <p
              className={styles.username}
            >{`${answer.user.firstName} answered:`}</p>
            <p> - {answer.value}</p>
          </div>
        ))}
      </section>
    );
  else return null;
};

export default Answers;
