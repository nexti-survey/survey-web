/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from "react";
import styles from "./Input.module.css";

const Input = ({
  label,
  type,
  value,
  name,
  error,
  onChange,
  onBlur,
  setValue,
  validate,
  ...props
}) => {
  return (
    <div className={styles.wrapper}>
      <label htmlFor={name} className={styles.label}>
        {label}
      </label>
      <input
        type={type}
        id={name}
        name={name}
        className={styles.input}
        onChange={onChange}
        onBlur={onBlur}
        value={value}
        {...props}
      ></input>
      {error && <p className={styles.erro}>{error}</p>}
    </div>
  );
};

export default Input;
