import React from "react";
import { Link } from "react-router-dom";
import styles from "./Header.module.css";

import { ReactComponent as SurveyLogo } from "../Assets/logo.svg";
import { UserContext } from "../UserContext";

const Header = () => {
  const { data, userLogout } = React.useContext(UserContext);

  return (
    <header className={styles.header}>
      <nav className={`${styles.nav} container`}>
        <Link className={styles.logo} to="/" aria-label="Survey">
          <SurveyLogo className={styles.logoImage} />
        </Link>
        <Link to="/">
          <p>Survey</p>
        </Link>
        {data ? (
          <>
            <span className={styles.login}>{data.firstName}</span>
            <button
              className={`btn-link ${styles.logout}`}
              onClick={(event) => {
                event.preventDefault();
                userLogout();
              }}
            >
              Logout
            </button>
          </>
        ) : (
          <Link className={styles.login} to="/login">
            Login
          </Link>
        )}
      </nav>
    </header>
  );
};

export default Header;
