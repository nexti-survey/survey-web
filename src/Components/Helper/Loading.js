import React from "react";
import styles from "./Loading.module.css";

import { ReactComponent as Logo } from "../../Assets/logo.svg";

const Loading = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.loading}>
        <Logo />
      </div>
    </div>
  );
};

export default Loading;
