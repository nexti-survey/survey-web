/* eslint-disable react/prop-types */
import React from "react";
import { Redirect, Route } from "react-router-dom";
import { UserContext } from "../../UserContext";

function ProtectedRoute({ component: Component, ...restOfProps }) {
  const { login, getUser } = React.useContext(UserContext);

  React.useEffect(() => {
    async function validate() {
      await getUser();
    }

    validate();
  }, []);

  return (
    <Route
      {...restOfProps}
      render={(props) =>
        login ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );
}

export default ProtectedRoute;
