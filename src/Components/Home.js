import React from "react";
import { QUESTIONS_GET } from "../api";
import useFetch from "../Hooks/useFetch";
import Loading from "./Helper/Loading";

import Question from "./Question";

const Home = () => {
  const { loading, request } = useFetch();

  const [questions, setQuestions] = React.useState(null);

  React.useEffect(() => {
    async function getQuestions() {
      const { url, options } = QUESTIONS_GET();

      const { json } = await request(url, options);

      setQuestions(json);
    }

    getQuestions();
  }, []);
  return (
    <section className="container mainContainer">
      {questions &&
        questions.map((question) => (
          <Question key={question._id} question={question} />
        ))}
      {loading && <Loading />}
    </section>
  );
};

export default Home;
