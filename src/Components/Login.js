import React from "react";
import { useHistory } from "react-router";
import Button from "./Forms/Button";
import Input from "./Forms/Input";
import useForm from "../Hooks/useForm";
import { UserContext } from "../UserContext";
import Error from "./Helper/Error";

import styles from "./Login.module.css";

const Login = () => {
  const username = useForm("", false);
  const password = useForm("", false);

  const history = useHistory();

  const { userLogin, error, login } = React.useContext(UserContext);

  async function handleSubmit(event) {
    event.preventDefault();

    if (username.validate()) await userLogin(username.value, password.value);

    // if (!login) {
    //   history.push("/");
    // }
  }

  React.useEffect(() => {
    if (login) history.push("/");
  }, [login]);

  return (
    <section className="container mainContainer">
      <h1 className="title">Login</h1>
      <form className={styles.login} onSubmit={handleSubmit}>
        <Input
          type="text"
          name="username"
          label="Username"
          autoComplete="off"
          autoFocus
          {...username}
        />
        <Input type="password" name="password" label="Password" {...password} />
        <Button>Login</Button>
        {error && <Error error={error} />}
      </form>
    </section>
  );
};

export default Login;
