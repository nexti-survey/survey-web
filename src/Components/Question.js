/* eslint-disable react/prop-types */
import React from "react";
import useForm from "../Hooks/useForm";
import Button from "./Forms/Button";
import Input from "./Forms/Input";

import { ReactComponent as Send } from "../Assets/send.svg";

import styles from "./Question.module.css";
import { ANSWERS_POST } from "../api";
import useFetch from "../Hooks/useFetch";
import Loading from "./Helper/Loading";
import Error from "./Helper/Error";
import { Link } from "react-router-dom";

const Question = ({ question }) => {
  const answer = useForm("", "answer");

  const { request, loading, error } = useFetch();

  const loadAnswer = (question) => {
    switch (question.answerType) {
      case "String":
        return <Input type="text" label={question.question} {...answer} />;
      case "Number":
        return <Input type="number" label={question.question} {...answer} />;
      case "Boolean":
        return (
          <div className={styles.radioGroup}>
            <label className={styles.radioButton}>
              <Input type="radio" value="0" name="radio" checked />
              <span>First</span>
            </label>
            <label className={styles.radioButton}>
              <Input type="radio" value="1" name="radio" />
              <span>Second</span>
            </label>
          </div>
        );
    }
  };

  async function handleSubmit(event) {
    event.preventDefault();

    if (answer.validate()) {
      const { url, options } = ANSWERS_POST(question._id, {
        value: answer.value,
      });

      await request(url, options);

      answer.setValue("");
    }
  }

  return (
    <div className={styles.questionWrapper}>
      <Link to={`/answers/${question._id}`} className={styles.answersLink}>Answers</Link>
      <form className={styles.form} onSubmit={handleSubmit}>
        {loadAnswer(question)}
        <Button className={styles.send}>
          <Send />
        </Button>
        {loading && <Loading />}
        {error && <Error error={error} />}
      </form>
    </div>
  );
};

export default Question;
