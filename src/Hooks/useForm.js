import React from "react";

const types = {
  answer: {
    regex: /^(.{30,})$/,
    message: "The answer must have at least 30 characters and cannot be null",
    callback: false,
  },
  number: {
    regex: /^(\d+)$/,
    message: "This value must contain only numbers",
    callback: false,
  },
};

const useForm = (initialValue = "", validationType, callback) => {
  const [value, setValue] = React.useState(initialValue);
  const [error, setError] = React.useState(null);

  function validate(value) {
    if (validationType === false) return true;
    if (value.length === 0) {
      setError("Required");
      return false;
    } else if (
      validationType in types &&
      !types[validationType].regex.test(value)
    ) {
      setError(types[validationType].message);
      return false;
    } else {
      setError(null);
      return types[validationType] && types[validationType].callback
        ? callback(value, setError)
        : true;
    }
  }

  async function onKeyUp() {
    if (validationType === false || !(validationType in types)) return true;
    if (types[validationType].callback) return await callback(value, setError);
    else return true;
  }

  function onChange({ target }) {
    if (error) validate(target.value);
    setValue(target.value);
  }

  return {
    value,
    setValue,
    onChange,
    onKeyUp,
    validate: () => validate(value),
    onBlur: () => validate(value),
    error,
  };
};

export default useForm;
