/* eslint-disable react/prop-types */
import React from "react";
import { useHistory } from "react-router";
import { USER_GET, USER_LOGIN_POST } from "./api";
import useFetch from "./Hooks/useFetch";

export const UserContext = React.createContext();

export const UserStorage = ({ children }) => {
  const [data, setData] = React.useState(null);
  const [login, setLogin] = React.useState(null);
  const [error, setError] = React.useState(null);

  const { loading, request } = useFetch();

  const history = useHistory();

  async function userLogin(username, password) {
    let { url, options } = USER_LOGIN_POST({
      username,
      password,
    });

    try {
      const { response, json } = await request(url, options, true);

      if (!response.ok) {
        setError(json.message);
        return;
      }

      let token = json.token;
      window.localStorage.setItem("token", token);
      setLogin(true);
      setError(null);

      await getUser();
    } catch (error) {
      setLogin(false);
      setError(error.message);
    }
  }

  async function getUser() {
    try {
      let { url, options } = USER_GET();

      const { response, json } = await request(url, options);

      if (!response.ok) {
        userLogout();
      }

      setData(json);
    } catch (error) {
      setLogin(false);
      setData(null);
      window.localStorage.removeItem("token");
    }
  }

  const userLogout = React.useCallback(
    async function () {
      setData(null);
      setLogin(false);
      setError(null);
      window.localStorage.removeItem("token");
      history.push("/login");
    },
    [history]
  );

  React.useEffect(() => {
    async function autoLogin() {
      const token = window.localStorage.getItem("token");
      if (token) {
        try {
          await getUser(token);
          setLogin(true);
        } catch (err) {
          userLogout();
        }
      } else {
        setLogin(false);
      }
    }
    autoLogin();
  }, [userLogout]);

  return (
    <UserContext.Provider
      value={{
        userLogin,
        getUser,
        userLogout,
        data,
        error,
        loading,
        login,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};
