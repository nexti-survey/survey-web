/* eslint-disable no-undef */
export const SURVEYAPI_URL =
  process.env.REACT_APP_ENV === "beta"
    ? "http://localhost:3100"
    : "https://api.survey.nexti.dev";

export function USER_LOGIN_POST(body) {
  return {
    url: `${SURVEYAPI_URL}/users/login`,
    options: {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    },
  };
}

export function USER_GET() {
  return {
    url: `${SURVEYAPI_URL}/users`,
    options: {
      headers: {
        "Content-Type": "application/json",
        "x-access-token": `${window.localStorage.getItem("token")}`,
      },
    },
  };
}

export function QUESTIONS_GET() {
  return {
    url: `${SURVEYAPI_URL}/questions`,
    options: {
      headers: {
        "Content-Type": "application/json",
        "x-access-token": `${window.localStorage.getItem("token")}`,
      },
    },
  };
}

export function QUESTION_GET(questionId) {
  return {
    url: `${SURVEYAPI_URL}/questions/${questionId}`,
    options: {
      headers: {
        "Content-Type": "application/json",
        "x-access-token": `${window.localStorage.getItem("token")}`,
      },
    },
  };
}

export function ANSWERS_GET(questionId) {
  return {
    url: `${SURVEYAPI_URL}/questions/${questionId}/answers`,
    options: {
      headers: {
        "Content-Type": "application/json",
        "x-access-token": `${window.localStorage.getItem("token")}`,
      },
    },
  };
}

export function ANSWERS_POST(questionId, body) {
  return {
    url: `${SURVEYAPI_URL}/questions/${questionId}/answers`,
    options: {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": `${window.localStorage.getItem("token")}`,
      },
      body: JSON.stringify(body),
    },
  };
}
